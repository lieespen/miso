# Miso



## About

Datasheet for the MCU here: 
https://www.espressif.com/sites/default/files/documentation/0a-esp8266ex_datasheet_en.pdf

- 32Mb flash
- 26MHz oscillator
- Baud 74880 bps

