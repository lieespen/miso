#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_err.h"
#include "esp_log.h"

#include  "driver/i2c.h"
 
#include "miso_sensors.h"
#include "miso_i2c.h"
#include "miso_config.h"

esp_err_t ms8607_pt_reset()
{
    return i2c_master_send_command(I2C_EXAMPLE_MASTER_NUM, MS8607_PT_ADDRESS, MS8607_PT_RESET);
}

esp_err_t ms8607_pt_get_coeffs(uint16_t *coeffs)
{   
    uint8_t *data = malloc(2*sizeof(uint8_t));
    int ret;
    for (int i = 0; i < 7; i++)
    {
        ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, MS8607_PT_ADDRESS, MS8607_PT_PROM_START+i*2, data, 2);
        if (ret != ESP_OK)
        {
            return ret;
        }
        *coeffs = (uint16_t)(*data << 8 | *(data+1));
        coeffs++;
    }

    return ret;
}

esp_err_t ms8607_pt_init_p_convertion()
{
    return i2c_master_send_command(I2C_EXAMPLE_MASTER_NUM, MS8607_PT_ADDRESS, MS8607_PT_D1_OSR_4096);
}

esp_err_t ms8607_pt_init_t_convertion()
{
    return i2c_master_send_command(I2C_EXAMPLE_MASTER_NUM, MS8607_PT_ADDRESS, MS8607_PT_D2_OSR_4096);
}

esp_err_t ms8607_pt_get_adc(uint32_t *adc_value)
{
    uint8_t *data = malloc(3*sizeof(uint8_t));
    int ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, MS8607_PT_ADDRESS, MS8607_ADC_START, data, 3);
    if (ret != ESP_OK)
    {
        return ret;
    }

    *adc_value = (*data << 16) | (*(data+1)<<8) | (*(data+2));

    return ret;
}

esp_err_t ms8607_pt_get_temp(float *temp)
{
    int ret;
    uint16_t *coeffs = malloc(7*sizeof(uint16_t));
    ret = ms8607_pt_get_coeffs(coeffs);

    ret = ms8607_pt_init_t_convertion();
    vTaskDelay(30 / portTICK_RATE_MS);
    if (ret != ESP_OK)
    {
        return ret;
    }

    uint32_t adc_value;
    ret = ms8607_pt_get_adc(&adc_value);
    if (ret != ESP_OK)
    {
        return ret;
    }

    int32_t dt;
    int32_t temp_int;

    dt = (int32_t)adc_value - ((int32_t)coeffs[5] << 8);
    temp_int = 2000 + ((int64_t)dt * (int64_t)coeffs[6] >> 23);
    int64_t T2;

    if (temp_int > 2000)
    {
        T2 =  ( 3 * ( (int64_t)dt  * (int64_t)dt  ) ) >> 33;
    }
    else
    {
        T2 =  ( 5 * ( (int64_t)dt  * (int64_t)dt  ) ) >> 38;
    }

    *temp = ((float)temp_int - T2) / 100;

    return ret;
}

esp_err_t apds_get_status(uint8_t *status)
{
    esp_err_t ret;
    ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, APDS_ADDRESS, 0x0, status, 1);
    return ret;
}

esp_err_t ccs811_get_status(uint8_t *status)
{
    esp_err_t ret;
    ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0x0, status, 1);
    return ret;
}

esp_err_t ccs811_get_eco2(uint8_t *data)
{
    esp_err_t ret;
    ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0x2, data, 2);
    return ret;
}

esp_err_t ccs811_get_error_code(uint8_t *error)
{
    esp_err_t ret;
    ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0xE0, error, 1);
    return ret;
}

esp_err_t ccs811_get_meas_mode(uint8_t *mode)
{
    esp_err_t ret;
    ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0x1, mode, 1);
    return ret;
}

esp_err_t ccs811_set_meas_mode(uint8_t mode)
{
    return i2c_master_write_register(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0x1, mode);
}

esp_err_t ccs811_get_hw_id(uint8_t *id)
{
    esp_err_t ret;
    ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0x20, id, 1);
    return ret;
}
