#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_system.h"

#include "nvs.h"

#include "miso_config.h"

esp_err_t get_nvs_miso_config(miso_config_t *conf)
{

    esp_err_t ret;
    nvs_handle miso_nvs_handle;
    ret = nvs_open("nvs", NVS_READWRITE, &miso_nvs_handle);
    if (ret != ESP_OK)
    {
        ESP_LOGI(TAG, "NVS open error");
        ESP_ERROR_CHECK(ret);
    }

    size_t req_size;
    ret = nvs_get_str(miso_nvs_handle, "sta_ssid", NULL, &req_size);

    conf->sta_password = "canningen";
    switch (ret)
    {
        case ESP_OK:
            nvs_get_str(miso_nvs_handle, "sta_ssid", conf->sta_ssid, &req_size);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            ESP_LOGI(TAG, "Variable not found");
            break;
        default:
            ESP_LOGI(TAG, "Other nvs error %s", esp_err_to_name(ret));
    }

    return ESP_OK;
}

/*
esp_err_t get_nvs_miso_str(char *key, char *value)
{
    esp_err_t ret;
    nvs_handle miso_nvs_handle;

    return ESP_OK;
}
*/
