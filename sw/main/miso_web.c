#include "esp_system.h"
#include "esp_http_server.h"
#include "esp_log.h"
#include "esp_err.h"

#include "miso_web.h"

#define MIN(a,b) (((a)<(b))?(a):(b))

const char resp[] = "\
<!DOCTYPE html>\n\
<html>\n\
  <head>\n\
    <title>Miso</title>\n\
  </head>\n\
  <body>\n\
    <h1>Miso</h1>\n\
    <form action='/' method='POST'>\n\
      <input type='text'>\n\
      <input type='submit'>\n\
    </form>\n\
  </body>\n\
</html>";


esp_err_t get_handler(httpd_req_t *req)
{
    /* Send a simple response */
    //const char resp[] = "<html><head><title>Miso</title></head><body><h1>Miso</h1><form action='/' method='POST'><input type='text'><input type='submit'></form></body></html>";
    httpd_resp_send(req, resp, strlen(resp));
    return ESP_OK;
}

esp_err_t post_handler(httpd_req_t *req)
{
    /* Destination buffer for content of HTTP POST request.
     * httpd_req_recv() accepts char* only, but content could
     * as well be any binary data (needs type casting).
     * In case of string data, null termination will be absent, and
     * content length would give length of string */
    char content[100];

    /* Truncate if content length larger than the buffer */
    size_t recv_size = MIN(req->content_len, sizeof(content));

    int ret = httpd_req_recv(req, content, recv_size);
    if (ret <= 0) {  /* 0 return value indicates connection closed */
        /* Check if timeout occurred */
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            /* In case of timeout one can choose to retry calling
             * httpd_req_recv(), but to keep it simple, here we
             * respond with an HTTP 408 (Request Timeout) error */
            httpd_resp_send_408(req);
        }
        /* In case of error, returning ESP_FAIL will
         * ensure that the underlying socket is closed */
        return ESP_FAIL;
    }

    /* Send a simple response */
    const char resp2[] = "Miso testing";
    httpd_resp_send(req, resp2, strlen(resp2));
    return ESP_OK;
}

static httpd_uri_t uri_get = {
    .uri      = "/",
    .method   = HTTP_GET,
    .handler  = get_handler,
    .user_ctx = NULL
};

static httpd_uri_t uri_post = {
    .uri      = "/",
    .method   = HTTP_POST,
    .handler  = post_handler,
    .user_ctx = NULL
};

esp_err_t start_webserver(void)
{
    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    /* Empty handle to esp_http_server */
    httpd_handle_t server = NULL;

    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK) {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &uri_get);
        httpd_register_uri_handler(server, &uri_post);
    }

    if (server == NULL)
    {
        return ESP_FAIL;
    }
    
    return ESP_OK;
}

void stop_webserver(httpd_handle_t server)
{
    if (server) {
        /* Stop the httpd server */
        httpd_stop(server);
    }
}


