#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_err.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_sleep.h"

#include "nvs_flash.h"
#include "nvs.h"

#include "driver/gpio.h"
#include "driver/i2c.h"

#include "mqtt_client.h"

#include "miso_wifi.h"
#include "miso_config.h"
#include "miso_web.h"
#include "miso_i2c.h"
#include "miso_nvs.h"
#include "miso_sensors.h"

#define SECOND 1e6


#define BUTTON_PUSHED 0

int WIFI_SAFE = 0;
int MQTT_SAFE = 0;

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            MQTT_SAFE = 1;
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void button_enable(void)
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = (1ULL << 0);
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);
}

static void init_gpio(void)
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = (1ULL << 4) | (1ULL << 15) | (1ULL << 5);
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);
}

void app_main()
{
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);

    button_enable();

    // Power misc sensors, move out from here later
    init_gpio();
    gpio_set_level(15, 1);
    gpio_set_level(5, 1);
    gpio_set_level(4, 0);

    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_LOGI(TAG, "No free NVS pages, erasing NVS");
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    miso_config_t conf;
    conf.sta_ssid = "NOT_IN_USE";
    conf.sta_password = "NOT_IN_USE";
    conf.mqtt_broker =  "192.168.1.189";
    conf.mqtt_topic = "sensor";
    //ret = get_nvs_miso_config(&conf);
    ESP_LOGI(TAG, "STA SSID %s", conf.sta_ssid);

    if (gpio_get_level(0) == BUTTON_PUSHED)
    {
        ESP_LOGI(TAG, "Button active, setting in Wifi in AP mode");
        wifi_init_softap();
        ret = start_webserver();
        if (ret != ESP_OK)
        {
            ESP_LOGE(TAG, "Failed to start webserver");
        }

    }
    else
    {
        wifi_init_sta((uint8_t)&conf.sta_ssid, (uint8_t)&conf.sta_password);

        ret = i2c_master_init();
        if (ret != ESP_OK)
        {
            ESP_LOGI(TAG, "Error initializing i2c master");
        }

        int ret; 
        uint8_t id;
        uint8_t ccs811_status;
        uint8_t *ccs811_data = malloc(6*sizeof(uint8_t));
        float tmp;
        vTaskDelay(5000 / portTICK_RATE_MS);
        i2c_master_write_register(I2C_EXAMPLE_MASTER_NUM, APDS_ADDRESS, 0x0, 0x2);
        vTaskDelay(1000 / portTICK_RATE_MS);

        /* This is just an example loop of readout apds light sensor and humidity from
            ms8601. Just keep it until dedicated functions are implemented.
        uint8_t h_test[3];
        h_test[0] = 0;
        h_test[1] = 0;
        h_test[2] = 0;
        while (true)
        {
            
            i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0x0, &id, 1);
            ESP_LOGI(TAG,"Read 0x0 val: %i", id);
            vTaskDelay(1000 / portTICK_RATE_MS);
            ccs811_set_meas_mode(32);
            vTaskDelay(1000 / portTICK_RATE_MS);
            ret = apds_get_status(&id);
            ESP_LOGI(TAG,"APDS 0x0 val: %i ret: %i", id, ret);
            vTaskDelay(1000 / portTICK_RATE_MS);

            i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, APDS_ADDRESS, 0xd, &id, 1);
            ESP_LOGI(TAG,"REG 0xd val: %i", id);
            vTaskDelay(200 / portTICK_RATE_MS);

            ret = i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, MS8607_H_ADDRESS, 0xf5, &h_test, 3);
            //ret = i2c_master_send_command(I2C_EXAMPLE_MASTER_NUM, MS8607_H_ADDRESS, 0x81);
            ESP_LOGI(TAG,"H sens: %i, %i, %i, ret: %i", h_test[0], h_test[1], h_test[2], ret);
            vTaskDelay(500 / portTICK_RATE_MS);

            i2c_master_read_from(I2C_EXAMPLE_MASTER_NUM, CCS811_ADDRESS, 0x1, &id, 1);
            ESP_LOGI(TAG,"Mode register 0x1 val: %i", id);
            vTaskDelay(1000 / portTICK_RATE_MS);
        }
        */

        ret = ms8607_pt_reset();
        
        float temp;
        ret =  ms8607_pt_get_temp(&temp);
        if (ret != ESP_OK)
        {
            ESP_LOGE(TAG, "Issue getting i2c temperature: %i", ret);
        }

        char sbuf[20];
        sprintf(sbuf, "%.2f", temp);
    
        char mqtt_broker[32];
        sprintf(mqtt_broker, "mqtt://%s:1883", conf.mqtt_broker);
        esp_mqtt_client_config_t mqtt_cfg = {
            .uri = mqtt_broker,
            .event_handle = mqtt_event_handler_cb,
        };

        esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
        if (ret != ESP_OK)
        {
            ESP_LOGI(TAG, "esp_mqtt_client_start returned %d", ret);
        }

        while (WIFI_SAFE == 0)
        {
            ESP_LOGI(TAG, "Loop while wifi is connecting.");
            vTaskDelay (200 / portTICK_RATE_MS);
        }
        ret = esp_mqtt_client_start(client);

        while (MQTT_SAFE == 0)
        {
            ESP_LOGI(TAG, "Loop while mqtt client is connecting");
            vTaskDelay(200 / portTICK_RATE_MS);
        }

        esp_mqtt_client_publish(client, "sensor", sbuf, 0, 1, 0);
        vTaskDelay(200 / portTICK_RATE_MS);
        esp_deep_sleep_set_rf_option(2);
        esp_deep_sleep(120*SECOND);
    }
}
