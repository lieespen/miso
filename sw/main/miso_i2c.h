#ifndef MISO_I2C_H
#define MISO_I2C_H


#define I2C_MASTER_SCL_IO           14                /*!< gpio number for I2C master clock */
#define I2C_MASTER_SDA_IO           2               /*!< gpio number for I2C master data  */
#define I2C_MASTER_NUM              I2C_NUM_0        /*!< I2C port number for master dev */
#define I2C_MASTER_TX_BUF_DISABLE   0                /*!< I2C master do not need buffer */
#define I2C_MASTER_RX_BUF_DISABLE   0                /*!< I2C master do not need buffer */

#define I2C_EXAMPLE_MASTER_SCL_IO           14                /*!< gpio number for I2C master clock */
#define I2C_EXAMPLE_MASTER_SDA_IO           2               /*!< gpio number for I2C master data  */
#define I2C_EXAMPLE_MASTER_NUM              I2C_NUM_0        /*!< I2C port number for master dev */
#define I2C_EXAMPLE_MASTER_TX_BUF_DISABLE   0                /*!< I2C master do not need buffer */
#define I2C_EXAMPLE_MASTER_RX_BUF_DISABLE   0                /*!< I2C master do not need buffer */

#define MPU6050_SENSOR_ADDR                 0x68             /*!< slave address for MPU6050 sensor */
#define MPU6050_CMD_START                   0x41             /*!< Command to set measure mode */
#define MPU6050_WHO_AM_I                    0x75             /*!< Command to read WHO_AM_I reg */
#define WRITE_BIT                           I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT                            I2C_MASTER_READ  /*!< I2C master read */
#define ACK_CHECK_EN                        0x1              /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS                       0x0              /*!< I2C master will not check ack from slave */
#define ACK_VAL                             0x0              /*!< I2C ack value */
#define NACK_VAL                            0x1              /*!< I2C nack value */
#define LAST_NACK_VAL                       0x2              /*!< I2C last_nack value */

#define GAS_A 0x5a
#define GAS_B 0x5b

esp_err_t i2c_master_init();
esp_err_t i2c_master_read_from(i2c_port_t i2c_num, uint8_t slave_addr, uint8_t reg_addr, uint8_t *data, size_t data_len);
esp_err_t i2c_master_send_command(i2c_port_t i2c_num, uint8_t slave_addr, uint8_t command);
esp_err_t i2c_master_write_register(i2c_port_t i2c_num, uint8_t slave_addr, uint8_t reg, uint8_t val);

esp_err_t i2c_example_master_ccs811_read(i2c_port_t i2c_num);

#endif
