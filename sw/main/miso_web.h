#ifndef MISO_WEB_H
#define MISO_WEB_H

#include "esp_http_server.h"

esp_err_t get_handler(httpd_req_t *req);
esp_err_t post_handler(httpd_req_t *req);
esp_err_t start_webserver(void);
void stop_webserver(httpd_handle_t server);

#endif 
