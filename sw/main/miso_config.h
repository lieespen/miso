#ifndef MISO_CONFIG_H
#define MISO_CONFIG_H

#define TAG     "Miso"

typedef struct
{
    char    *sta_ssid;
    char    *sta_password;
    char    *mqtt_broker;
    char    *mqtt_topic;
} miso_config_t;

#endif
