#ifndef MISO_SENSORS_H
#define MISO_SENSORS_H

#define MS8607_PT_ADDRESS 0x76
#define MS8607_PT_RESET 0x1E
#define MS8607_PT_PROM_START 0xA0
#define MS8607_ADC_START 0x0
#define MS8607_PT_D1_OSR_4096 0x48
#define MS8607_PT_D2_OSR_4096 0x58

#define MS8607_H_ADDRESS 0x40

#define CCS811_ADDRESS 0x5B
#define APDS_ADDRESS 0x52

esp_err_t ms8607_pt_reset();
esp_err_t ms8607_pt_get_coeffs(uint16_t *coeffs);
esp_err_t ms8607_pt_init_p_convertion();
esp_err_t ms8607_pt_init_t_convertion();
esp_err_t ms8607_pt_get_adc(uint32_t *adc_value);
esp_err_t ms8607_pt_get_temp(float *temp);

esp_err_t ccs811_get_status(uint8_t *status);
esp_err_t ccs811_get_eco2(uint8_t *data);
esp_err_t ccs811_get_error_code(uint8_t *error);
esp_err_t ccs811_get_meas_mode(uint8_t *mode);
esp_err_t ccs811_set_meas_mode(uint8_t mode);
esp_err_t ccs811_get_hw_id(uint8_t *id);

esp_err_t apds_get_status(uint8_t *status);
#endif /* MISO_SENSORS_H */
