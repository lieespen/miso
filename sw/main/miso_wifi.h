#ifndef MISO_WIFI_H
#define MISO_WIFI_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"

extern int WIFI_SAFE;
void wifi_init_sta(uint8_t wifi_ssid[32], uint8_t wifi_password[64]);
void wifi_init_softap(void);

#endif
