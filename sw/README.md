# Miso

Miso i2c sensor board

## How to build and flash

1. First setup SDK and toolchain
    - Install prerequisites `sudo apt-get install git wget libncurses-dev flex bison gperf python python-pip python-setuptools python-serial python-click python-cryptography python-future python-pyparsing python-pyelftools cmake ninja-build ccache libffi-dev libssl-dev`
    - Get SDK
        `mkdir ~/esp && cd ~/esp`
        `git clone https://github.com/espressif/ESP8266_RTOS_SDK.git`
    - Download toolchain: `wget -c https://dl.espressif.com/dl/xtensa-lx106-elf-linux64-1.22.0-100-ge567ec7-5.2.0.tar.gz -O - | tar -xz`

2. Get miso application:
    `git clone https://gitlab.com/lieespen/miso`

3. Set environmental variables:
    `export IDF_PATH=~/esp/ESP8266_RTOS_SDK`
    `export PATH=~/esp/xtensa-lx106-elf/bin/:$PATH`

4. Build the app:
    Go into directory sw and build app with `make` 

5. Flash the app using python utility or in make command with `make flash`



